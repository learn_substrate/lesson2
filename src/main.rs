//exercise 1
// fn change_value(input: u32, output: &mut u32) -> u32 {
//     if input == 1 {
//         *output = 3;
//     } else {
//         *output = 4;
//     }
//     *output
// }

// fn main() {
//     let x = change_value(10, &mut 20);
//     println!("xxx {} ", x);
// }

// exercise 2
// fn vector_is_prime(num: u64, p: &Vec<u64>) -> bool {
//     for i in p {
//         if num > *i && num % i != 0 {
//             false;
//         }
//     }
//     true
// }

// fn main() {
//     let mut count: u32 = 1;
//     let mut num: u64 = 1;
//     let mut primes: Vec<u64> = Vec::new();
//     primes.push(2);
//     while count < 10 {
//         num += 2;
//         if vector_is_prime(num, &primes) {
//             count += 1;
//             primes.push(num);
//         }
//     }
//     println!("{:?}", primes);
// }

// exercise 3
// fn main() {
//     let mut values = vec![10, 11, 12];
//     let v = &mut values;
//     let mut max = 0;
//     for n in v.into_iter() {
//         max = std::cmp::max(max, *n);
//     }
//     println!("max is {}", max);
//     println!("converting  ");
//     for n in v {
//         *n = 100 * (*n) / max;
//     }
//     println!("values: {:?}", values);
// }

// exercise 4
pub fn test(mut a: &mut Vec<u8>) -> (Vec<u8>, i32) {
    let mut b: Vec<u8> = Vec::new();
    let mut c: u8 = 0;
    loop {
        if a.len() == 0 {
            break;
        }
        let d = a.pop().unwrap();
        c = c + d;
        b.push(d);
    }
    (b, c as i32)
}

fn main() {
    // let mut a = &mut vec![1, 2, 3, 4, 5];
    // let mut i = 0;
    // let c = 0;
    // loop {
    //     let (a, c) = test(a);
    //     if i >= a.len() {
    //         break;
    //     }
    // }

    // fix wrong logic
    let a = vec![1, 2, 3, 4, 5];
    println!("old {:?}", a);
    let y: Vec<_> = a.into_iter().rev().collect();
    println!("rev {:?}", y);
}
